﻿using System;
using System.Collections.Generic;
using MvvmCross.Platforms.Ios.Binding.Views;
using UIKit;

namespace MySpectrum.Interfaces
{
    public interface IListViewDelegate
    {
        UITableView TableView { get; }

        IEnumerable<ICellMapping> ViewModelMappings { get; }

        bool ShouldSupportRefreshControl { get; }

        MvxTableViewSource CreateTableViewSource(UITableView tableView, Dictionary<Type, ICellMapping> map);
    }
}
