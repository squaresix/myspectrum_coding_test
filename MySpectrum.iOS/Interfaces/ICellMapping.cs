﻿using System;
using UIKit;

namespace MySpectrum.Interfaces
{
    public interface ICellMapping
    {
        string Key { get; }
        UINib Nib { get; }
        Type CellViewModelType { get; }
    }
}
