﻿using System;
using System.Linq;
using MySpectrum.Core.Interfaces;
using UIKit;

namespace MySpectrum.Services
{
    public class AlertService : IAlertService
    {
        public void ShowMessage(string message)
        {
            var alertController = UIAlertController.Create("", message, UIAlertControllerStyle.Alert);
            alertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

            var window = UIApplication.SharedApplication.KeyWindow;
            var vc = window.RootViewController;
            while (vc.PresentedViewController != null)
            {
                vc = vc.PresentedViewController;
            }

            if (vc is UINavigationController navController)
            {
                vc = navController.ViewControllers.Last();
            }

            vc?.PresentViewController(alertController, true, null);
        }
    }
}
