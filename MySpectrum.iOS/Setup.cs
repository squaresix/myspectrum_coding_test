﻿using MvvmCross;
using MvvmCross.Platforms.Ios.Core;
using MvvmCross.ViewModels;
using MySpectrum.Core;
using MySpectrum.Core.Interfaces;
using MySpectrum.Services;

namespace MySpectrum
{
    public class Setup : MvxIosSetup<App>
    {
        protected override void InitializeFirstChance()
        {
            base.InitializeFirstChance();

            Mvx.IoCProvider.RegisterType<IAlertService, AlertService>();
        }
    }
}
