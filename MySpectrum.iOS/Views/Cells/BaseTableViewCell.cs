﻿using System;
using MvvmCross.Platforms.Ios.Binding.Views;

namespace MySpectrum.Views.Cells
{
    public class BaseTableViewCell : MvxTableViewCell
    {
        public BaseTableViewCell(IntPtr handle) : base(handle)
        { }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            ApplyBindings();
        }

        protected virtual void ApplyBindings() { }
    }
}
