// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace MySpectrum.Views.Cells
{
	[Register ("UserTableViewCell")]
	partial class UserTableViewCell
	{
		[Outlet]
		UIKit.UILabel UserLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (UserLabel != null) {
				UserLabel.Dispose ();
				UserLabel = null;
			}
		}
	}
}
