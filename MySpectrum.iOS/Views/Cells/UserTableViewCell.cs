﻿using System;

using Foundation;
using MvvmCross.Binding.BindingContext;
using MySpectrum.Core.ViewModels.Cells;
using UIKit;

namespace MySpectrum.Views.Cells
{
    public partial class UserTableViewCell : BaseTableViewCell
    {
        public static readonly NSString Key = new NSString(nameof(UserTableViewCell));
        public static readonly UINib Nib;

        static UserTableViewCell()
        {
            Nib = UINib.FromName(nameof(UserTableViewCell), NSBundle.MainBundle);
        }

        protected UserTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        protected override void ApplyBindings()
        {
            base.ApplyBindings();

            var set = this.CreateBindingSet<UserTableViewCell, UserCellViewModel>();
            set.Bind(UserLabel).For(p => p.Text).To(vm => vm.UserId);
            set.Apply();
        }
    }
}
