﻿using System;
using System.Collections.Generic;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.Platforms.Ios.Views;
using MySpectrum.Core.ViewModels.Base;
using MySpectrum.Interfaces;
using UIKit;

namespace MySpectrum.Views.Base
{
    public abstract class BaseListViewController<TViewModel> : BaseViewController<TViewModel>, IListViewDelegate where TViewModel : BaseViewModel
    {
        private readonly Dictionary<Type, ICellMapping> CellViewModelMap = new Dictionary<Type, ICellMapping>();
        protected MvxTableViewSource _source;
        private MvxUIRefreshControl _refreshControl;
        public virtual float EstimatedRowHeight => 50f;

        public abstract UITableView TableView { get; }
        public abstract IEnumerable<ICellMapping> ViewModelMappings { get; }
        public virtual bool ShouldSupportRefreshControl => false;
        public virtual MvxTableViewSource CreateTableViewSource(UITableView tableView, Dictionary<Type, ICellMapping> map) => new PolymorphicTableViewSource(tableView, map);

        public BaseListViewController(IntPtr handle) : base(handle)
        { }

        public BaseListViewController(string nibName) : base(nibName)
        { }

        public override void ViewDidLoad()
        {
            foreach (var cellMapping in ViewModelMappings)
            {
                TableView.RegisterNibForCellReuse(cellMapping.Nib, cellMapping.Key);
                CellViewModelMap[cellMapping.CellViewModelType] = cellMapping;
            }

            if (ShouldSupportRefreshControl)
            {
                _refreshControl = new MvxUIRefreshControl();
                TableView.AddSubview(_refreshControl);
                TableView.SendSubviewToBack(_refreshControl);
            }

            _source = CreateTableViewSource(TableView, CellViewModelMap);

            TableView.Source = _source;
            TableView.TableFooterView = new UIView();
            TableView.TableHeaderView = new UIView();
            TableView.BackgroundColor = UIColor.White;
            TableView.RowHeight = UITableView.AutomaticDimension;
            TableView.EstimatedRowHeight = EstimatedRowHeight;

            base.ViewDidLoad();
        }

        protected override void ApplyBindings()
        {
            base.ApplyBindings();

            this.CreateBinding(_source).For(s => s.ItemsSource).To((BaseListViewModel vm) => vm.ListItems).Apply();
            this.CreateBinding(_source).For(s => s.SelectionChangedCommand).To((BaseListViewModel vm) => vm.ItemSelectedCommand).Apply();

            if (ShouldSupportRefreshControl)
            {
                this.CreateBinding(_refreshControl).For(r => r.IsRefreshing).To((BaseListViewModel vm) => vm.IsRefreshing).Apply();
                this.CreateBinding(_refreshControl).For(r => r.RefreshCommand).To((BaseListViewModel vm) => vm.ReloadCommand).Apply();
                this.CreateBinding(_refreshControl).For(r => r.Message).To((BaseListViewModel vm) => vm.BusyMessage).Apply();
            }
        }
    }
}
