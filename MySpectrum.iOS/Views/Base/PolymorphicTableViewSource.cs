﻿using System;
using System.Collections.Generic;
using Foundation;
using MvvmCross;
using MvvmCross.Logging;
using MvvmCross.Platforms.Ios.Binding.Views;
using MySpectrum.Interfaces;
using UIKit;

namespace MySpectrum.Views.Base
{
    public class PolymorphicTableViewSource : MvxTableViewSource
    {
        protected readonly Dictionary<Type, ICellMapping> Mappings;

        public PolymorphicTableViewSource(UITableView tableView, Dictionary<Type, ICellMapping> mappings) : base(tableView)
        {
            Mappings = mappings;
            DeselectAutomatically = true;
            tableView.ShowsVerticalScrollIndicator = false;
            tableView.ShowsHorizontalScrollIndicator = false;
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            try
            {
                var cell = tableView.DequeueReusableCell(Mappings[item.GetType()].Key, indexPath);
                return cell;
            }
            catch (KeyNotFoundException)
            {
                Mvx.IoCProvider.Resolve<IMvxLog>().Error($"Error retrieving a Cell Mapping for ViewModelType {item.GetType().Name}");
                throw;
            }
            catch (Exception)
            {
                Mvx.IoCProvider.Resolve<IMvxLog>().Error($"Error Dequeueing ReusableCell Cell for ViewModelType {item.GetType().Name}");
                throw;
            }
        }
    }
}
