﻿using System;
using CoreGraphics;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Views;
using MvvmCross.ViewModels;
using MySpectrum.Core.ViewModels.Base;
using UIKit;

namespace MySpectrum.Views.Base
{
    public class BaseViewController<TViewModel> : MvxBaseViewController<TViewModel> where TViewModel : class, IMvxViewModel
    {
        protected virtual bool ShowsNavigationBar => true;
        protected virtual bool AddKeyboardGesture => false;

        private UITapGestureRecognizer _dismissKeyboardGesture;

        public BaseViewController()
        {
        }

        public BaseViewController(IntPtr handle) : base(handle)
        {
        }

        public BaseViewController(string nibName) : base(nibName, null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            ApplyBindings();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            if (NavigationController != null)
            {
                NavigationController.NavigationBarHidden = !ShowsNavigationBar;
            }

            if (AddKeyboardGesture)
            {
                _dismissKeyboardGesture = new UITapGestureRecognizer(HandleDismissKeyboard);
                _dismissKeyboardGesture.NumberOfTapsRequired = 1;
                View.AddGestureRecognizer(_dismissKeyboardGesture);
            }
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            HandleDismissKeyboard();
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);

            if (AddKeyboardGesture)
            {
                View.RemoveGestureRecognizer(_dismissKeyboardGesture);
                _dismissKeyboardGesture = null;
            }
        }

        protected virtual void ApplyBindings()
        {
            this.CreateBinding(this).For(t => t.Title).To((BaseViewModel vm) => vm.ViewTitle).Apply();
        }

        private void HandleDismissKeyboard()
        {
            View.EndEditing(true);
        }
    }
}
