﻿using System;
using MySpectrum.Core.Interfaces;
using MySpectrum.Interfaces;
using UIKit;

namespace MySpectrum.Views.Base
{
    public class CellMapping<T> : ICellMapping where T : ICellViewModel
    {
        public CellMapping(string key, UINib nib)
        {
            Key = key;
            Nib = nib;
        }

        public string Key { get; private set; }
        public UINib Nib { get; private set; }
        public Type CellViewModelType => typeof(T);
    }
}
