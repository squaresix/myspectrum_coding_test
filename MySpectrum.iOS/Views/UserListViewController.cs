﻿using System;
using System.Collections.Generic;
using MySpectrum.Core.ViewModels;
using MySpectrum.Core.ViewModels.Cells;
using MySpectrum.Interfaces;
using MySpectrum.Views.Base;
using MySpectrum.Views.Cells;
using UIKit;

namespace MySpectrum.Views
{
    public partial class UserListViewController : BaseListViewController<UserListViewModel>
    {
        public override UITableView TableView => UsersTableView;

        public override IEnumerable<ICellMapping> ViewModelMappings => new List<ICellMapping> {
            new CellMapping<UserCellViewModel>(UserTableViewCell.Key, UserTableViewCell.Nib)
        };

        public UserListViewController() : base(nameof(UserListViewController))
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            UsersTableView.EstimatedRowHeight = 50f;
            UsersTableView.RowHeight = UITableView.AutomaticDimension;

            NavigationItem.RightBarButtonItem = new UIBarButtonItem(UIBarButtonSystemItem.Add, HandleAddUser);
        }

        private void HandleAddUser(object sender, EventArgs e)
        {
            ViewModel.AddUserCommand.ExecuteAsync();
        }
    }
}

