﻿using System;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MySpectrum.Core.ViewModels;
using MySpectrum.Views.Base;
using UIKit;

namespace MySpectrum.Views.Account
{
    [MvxModalPresentation]
    public partial class CreateAccountViewController : BaseViewController<CreateAccountViewModel>
    {
        protected override bool AddKeyboardGesture => true;

        public CreateAccountViewController() : base(nameof(CreateAccountViewController))
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            CreateAccountBtn.Layer.CornerRadius = 8f;
        }

        protected override void ApplyBindings()
        {
            base.ApplyBindings();

            var set = this.CreateBindingSet<CreateAccountViewController, CreateAccountViewModel>();
            set.Bind(CreateAccountBtn).To(vm => vm.CreateAccountCommand);
            set.Bind(SignBtn).To(vm => vm.SignUpCommand);
            set.Bind(SignBtn).For(BindingNames.ButtonTitle).To(vm => vm.CancelButtonText);
            set.Bind(UsernameTxt).To(vm => vm.Username);
            set.Bind(PasswordTxt).To(vm => vm.Password);
            set.Bind(ConfirmTxt).To(vm => vm.ConfirmedPassword);
            set.Apply();
        }
    }
}

