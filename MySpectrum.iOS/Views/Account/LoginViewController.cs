﻿using System;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MySpectrum.ViewModels;
using MySpectrum.Views.Base;
using UIKit;

namespace MySpectrum.Views.Account
{
    [MvxRootPresentation(WrapInNavigationController = true)]
    public partial class LoginViewController : BaseViewController<LoginViewModel>
    {
        protected override bool ShowsNavigationBar => false;
        protected override bool AddKeyboardGesture => true;

        public LoginViewController() : base(nameof(LoginViewController))
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            SignInButton.Layer.CornerRadius = 8f;
        }

        protected override void ApplyBindings()
        {
            base.ApplyBindings();

            var set = this.CreateBindingSet<LoginViewController, LoginViewModel>();
            set.Bind(SignInButton).To(vm => vm.SignInCommand);
            set.Bind(CreateAccountButton).To(vm => vm.CreateAccountCommand);
            set.Bind(UserNameTxtField).To(vm => vm.Username);
            set.Bind(PasswordTxtField).To(vm => vm.Password);
            set.Apply();
        }
    }
}

