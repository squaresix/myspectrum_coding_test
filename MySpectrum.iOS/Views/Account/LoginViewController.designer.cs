// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace MySpectrum.Views.Account
{
	[Register ("LoginViewController")]
	partial class LoginViewController
	{
		[Outlet]
		UIKit.UIButton CreateAccountButton { get; set; }

		[Outlet]
		UIKit.UIScrollView MainScrollView { get; set; }

		[Outlet]
		UIKit.UITextField PasswordTxtField { get; set; }

		[Outlet]
		UIKit.UIButton SignInButton { get; set; }

		[Outlet]
		UIKit.UITextField UserNameTxtField { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (MainScrollView != null) {
				MainScrollView.Dispose ();
				MainScrollView = null;
			}

			if (UserNameTxtField != null) {
				UserNameTxtField.Dispose ();
				UserNameTxtField = null;
			}

			if (PasswordTxtField != null) {
				PasswordTxtField.Dispose ();
				PasswordTxtField = null;
			}

			if (SignInButton != null) {
				SignInButton.Dispose ();
				SignInButton = null;
			}

			if (CreateAccountButton != null) {
				CreateAccountButton.Dispose ();
				CreateAccountButton = null;
			}
		}
	}
}
