// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace MySpectrum.Views.Account
{
	[Register ("CreateAccountViewController")]
	partial class CreateAccountViewController
	{
		[Outlet]
		UIKit.UITextField ConfirmTxt { get; set; }

		[Outlet]
		UIKit.UIButton CreateAccountBtn { get; set; }

		[Outlet]
		UIKit.UIScrollView MainScrollView { get; set; }

		[Outlet]
		UIKit.UITextField PasswordTxt { get; set; }

		[Outlet]
		UIKit.UIButton SignBtn { get; set; }

		[Outlet]
		UIKit.UITextField UsernameTxt { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (MainScrollView != null) {
				MainScrollView.Dispose ();
				MainScrollView = null;
			}

			if (UsernameTxt != null) {
				UsernameTxt.Dispose ();
				UsernameTxt = null;
			}

			if (PasswordTxt != null) {
				PasswordTxt.Dispose ();
				PasswordTxt = null;
			}

			if (ConfirmTxt != null) {
				ConfirmTxt.Dispose ();
				ConfirmTxt = null;
			}

			if (CreateAccountBtn != null) {
				CreateAccountBtn.Dispose ();
				CreateAccountBtn = null;
			}

			if (SignBtn != null) {
				SignBtn.Dispose ();
				SignBtn = null;
			}
		}
	}
}
