﻿using System;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using MySpectrum.ViewModels;
using UIKit;

namespace Views
{
    [MvxRootPresentation(WrapInNavigationController = false)]
    public partial class LaunchViewController : MvxViewController<LaunchViewModel>
    {
        public LaunchViewController() : base(nameof(LaunchViewController), null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }
    }
}

