﻿using System;
namespace MySpectrum.Core.Models
{
    public class UserAccountModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
