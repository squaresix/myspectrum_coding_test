﻿using System;
namespace MySpectrum.Core.Models
{
    public class CreateAccountParameter
    {
        public bool IsAuthenticated { get; set; }
    }
}
