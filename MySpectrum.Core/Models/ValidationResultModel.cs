﻿using System;
using System.Collections.Generic;

namespace MySpectrum.Core.Models
{
    public class ValidationResultModel
    {
        public bool IsValid { get; set; }
        public IEnumerable<string> Errors { get; set; }
    }
}
