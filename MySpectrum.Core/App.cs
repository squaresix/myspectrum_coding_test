﻿using System;
using Akavache;
using MvvmCross.IoC;
using MvvmCross.ViewModels;

namespace MySpectrum.Core
{
    public class App : MvxApplication
    {
        private const string CACHE_NAME = "MySpectrumApp";

        public override void Initialize()
        {
            Registrations.Start(CACHE_NAME);
            BlobCache.ApplicationName = CACHE_NAME;
            BlobCache.Secure.Vacuum();

            CreatableTypes().EndingWith("Service").AsInterfaces().RegisterAsLazySingleton();

            RegisterCustomAppStart<AppStart>();
        }
    }
}
