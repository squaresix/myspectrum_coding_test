﻿using System;
using System.Threading.Tasks;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace MySpectrum.ViewModels
{
    public class LaunchViewModel : MvxNavigationViewModel
    {
        public LaunchViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService) : base(logProvider, navigationService)
        {
        }

        public override async void Start()
        {
            base.Start();

            // This is just a placeholder to simulate any logic that needs to be loaded or initialized before the app loads in
            await Task.Delay(2000);

            await NavigationService.Navigate<LoginViewModel>();
        }
    }
}
