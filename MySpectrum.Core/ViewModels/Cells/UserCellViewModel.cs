﻿using System;
using MySpectrum.Core.ViewModels.Base.Cells;

namespace MySpectrum.Core.ViewModels.Cells
{
    public class UserCellViewModel : BaseCellViewModel
    {
        public string UserId { get; set; }

        public UserCellViewModel(string userId)
        {
            UserId = userId;
        }
    }
}
