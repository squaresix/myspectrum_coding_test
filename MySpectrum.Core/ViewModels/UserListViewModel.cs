﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MySpectrum.Core.Interfaces;
using MySpectrum.Core.Models;
using MySpectrum.Core.Tools;
using MySpectrum.Core.ViewModels.Base;
using MySpectrum.Core.ViewModels.Base.Cells;
using MySpectrum.Core.ViewModels.Cells;

namespace MySpectrum.Core.ViewModels
{
    public class UserListViewModel : BaseListViewModel
    {
        private readonly ICacheService _cacheService;

        private IMvxAsyncCommand _addUserCommand;
        public IMvxAsyncCommand AddUserCommand => _addUserCommand ?? (_addUserCommand = new MvxAsyncCommand(HandleAddUser));

        public UserListViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService, ICacheService cacheService) : base(logProvider, navigationService)
        {
            _cacheService = cacheService;

            ViewTitle = Constants.Strings.CurrentUsers;
        }

        public override async void Start()
        {
            base.Start();

            var users = await _cacheService.GetAllUsersAsync();
            var cells = new List<UserCellViewModel>();
            foreach (var u in users)
            {
                cells.Add(new UserCellViewModel(u.Username));
            }

            ListItems = new MvxObservableCollection<BaseCellViewModel>(cells);
            await RaisePropertyChanged(() => ListItems);
        }

        private Task HandleAddUser()
        {
            var param = new CreateAccountParameter
            {
                IsAuthenticated = true
            };
            return NavigationService.Navigate<CreateAccountViewModel, CreateAccountParameter>(param);
        }
    }
}
