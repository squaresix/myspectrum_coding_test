﻿using System;
using MvvmCross.ViewModels;
using MySpectrum.Core.Interfaces;

namespace MySpectrum.Core.ViewModels.Base.Cells
{
    public abstract class BaseCellViewModel : MvxViewModel, ICellViewModel
    {
        public BaseCellViewModel()
        {
        }
    }
}
