﻿using System;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace MySpectrum.Core.ViewModels.Base
{
    public class BaseViewModel : MvxNavigationViewModel
    {
        public string ViewTitle { get; set; }

        public BaseViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService) : base(logProvider, navigationService)
        {
        }
    }

    public abstract class BaseViewModelParemeter<TParameter> : BaseViewModel, IMvxViewModel<TParameter>
    {
        protected BaseViewModelParemeter(IMvxLogProvider logProvider, IMvxNavigationService navigationService) : base(logProvider, navigationService)
        {
        }

        public abstract void Prepare(TParameter parameter);
    }

    public abstract class BaseViewModel<TParameter> : BaseViewModelParemeter<TParameter>
    {
        public TParameter Parameter { get; private set; }

        protected BaseViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService) : base(logProvider, navigationService)
        {
        }

        public override void Prepare(TParameter parameter)
        {
            Parameter = parameter;
        }
    }
}
