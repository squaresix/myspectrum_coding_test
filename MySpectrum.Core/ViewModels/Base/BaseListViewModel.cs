﻿using System;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MySpectrum.Core.ViewModels.Base.Cells;

namespace MySpectrum.Core.ViewModels.Base
{
    public abstract class BaseListViewModel<TInit> : BaseViewModel<TInit>
    {
        public virtual bool HandleSelection => false;
        public virtual bool SupportPullRefresh => true;
        public virtual string BusyMessage => "Reloading data";

        public virtual MvxObservableCollection<BaseCellViewModel> ListItems { get; set; }
        public bool IsRefreshing { get; set; }

        public virtual IMvxAsyncCommand<BaseCellViewModel> ItemSelectedCommand { get; private set; }

        public IMvxCommand ReloadCommand
        {
            get
            {
                return new MvxAsyncCommand(DoReloadDataAsync, () => SupportPullRefresh);
            }
        }

        protected BaseListViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService) : base(logProvider, navigationService)
        {
            ItemSelectedCommand = new MvxAsyncCommand<BaseCellViewModel>(DoSelectItemAsync);
        }

        public virtual Task ItemSelectedAsync(BaseCellViewModel item) => Task.FromResult(0);

        protected virtual Task ReloadDataAsync() => Task.FromResult(0);

        private async Task DoSelectItemAsync(BaseCellViewModel item)
        {
            if (!HandleSelection)
                return;

            await ItemSelectedAsync(item);
        }

        private async Task DoReloadDataAsync()
        {
            IsRefreshing = true;

            await ReloadDataAsync();

            IsRefreshing = false;
        }
    }

    public class BaseListViewModel : BaseViewModel
    {
        public bool IsRefreshing { get; set; }

        public virtual bool HandleSelection => false;
        public virtual bool SupportPullRefresh => true;
        public virtual string BusyMessage => "Reloading data";

        public virtual IMvxAsyncCommand<BaseCellViewModel> ItemSelectedCommand { get; private set; }
        public virtual MvxObservableCollection<BaseCellViewModel> ListItems { get; set; }

        public IMvxCommand ReloadCommand
        {
            get
            {
                return new MvxAsyncCommand(DoReloadDataAsync, () => SupportPullRefresh);
            }
        }

        protected BaseListViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService) : base(logProvider, navigationService)
        {
            ItemSelectedCommand = new MvxAsyncCommand<BaseCellViewModel>(DoSelectItemAsync);
        }

        public virtual Task ItemSelectedAsync(BaseCellViewModel item) => Task.FromResult(0);

        protected virtual Task ReloadDataAsync() => Task.FromResult(0);

        private async Task DoSelectItemAsync(BaseCellViewModel item)
        {
            if (!HandleSelection)
                return;

            await ItemSelectedAsync(item);
        }

        private async Task DoReloadDataAsync()
        {
            IsRefreshing = true;

            await ReloadDataAsync();

            IsRefreshing = false;
        }
    }
}
