﻿using System;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MySpectrum.Core.Interfaces;
using MySpectrum.Core.Models;
using MySpectrum.Core.Tools;
using MySpectrum.Core.ViewModels.Base;

namespace MySpectrum.Core.ViewModels
{
    public class CreateAccountViewModel : BaseViewModel<CreateAccountParameter>
    {
        private readonly IValidationService _validationService;
        private readonly ICacheService _cacheService;
        private readonly IAlertService _alertService;

        private bool _isUserAuthenticated;

        public string Username { get; private set; }
        public string Password { get; private set; }
        public string ConfirmedPassword { get; private set; }

        private string _cancelButtonText;
        public string CancelButtonText
        {
            get { return _cancelButtonText; }
            set {  SetProperty(ref _cancelButtonText, value); }
        }

        private IMvxAsyncCommand _createAccountCommand;
        public IMvxAsyncCommand CreateAccountCommand => _createAccountCommand ?? (_createAccountCommand = new MvxAsyncCommand(HandleCreateAccount));

        private IMvxAsyncCommand _signUpCommand;
        public IMvxAsyncCommand SignUpCommand => _signUpCommand ?? (_signUpCommand = new MvxAsyncCommand(HandleSignup));

        public CreateAccountViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService, IValidationService validationService, ICacheService cacheService, IAlertService alertService) : base(logProvider, navigationService)
        {
            _validationService = validationService;
            _cacheService = cacheService;
            _alertService = alertService;

            ViewTitle = Constants.Strings.SignUp;
            CancelButtonText = Constants.Strings.SignInHere;
        }

        public override void Prepare(CreateAccountParameter parameter)
        {
            base.Prepare();

            _isUserAuthenticated = parameter.IsAuthenticated;
            if (_isUserAuthenticated)
            {
                CancelButtonText = Constants.Strings.Cancel;
            }
        }

        private async Task HandleCreateAccount()
        {
            if (IsValidInputs())
            {
                var user = new UserAccountModel
                {
                    Username = this.Username,
                    Password = this.Password
                };

                // Check if user already exists
                var cachedUser = await _cacheService.GetUserAsync(Username);
                if (cachedUser != null)
                {
                    _alertService.ShowMessage(Constants.Strings.AccountAlreadyExists);
                    return;
                }

                await _cacheService.SetUserAsync(user);
                await NavigationService.Close(this);

                if (!_isUserAuthenticated)
                {
                    await NavigationService.Navigate<UserListViewModel>();
                }
            }
        }

        private Task HandleSignup()
        {
            return NavigationService.Close(this);
        }

        private bool IsValidInputs()
        {
            if (string.IsNullOrEmpty(Username?.Trim()))
            {
                _alertService.ShowMessage(Constants.Strings.EmailRequired);
                return false;
            }

            if (string.IsNullOrEmpty(Password?.Trim()))
            {
                _alertService.ShowMessage(Constants.Strings.PasswordRequired);
                return false;
            }

            if (string.IsNullOrEmpty(ConfirmedPassword?.Trim()))
            {
                _alertService.ShowMessage(Constants.Strings.PasswordsDontMatch);
                return false;
            }

            if (Password != ConfirmedPassword)
            {
                _alertService.ShowMessage(Constants.Strings.PasswordsDontMatch);
                return false;
            }

            if (!_validationService.IsValidEmail(Username))
            {
                _alertService.ShowMessage(Constants.Strings.EnterValidEmail);
                return false;
            }

            var passwordResult = _validationService.ValidatePassword(Password);
            if (!passwordResult.IsValid)
            {
                _alertService.ShowMessage(string.Join("\n", passwordResult.Errors));
                return false;
            }

            return true;
        }
    }
}
