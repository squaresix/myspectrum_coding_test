﻿using System;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MySpectrum.Core.Interfaces;
using MySpectrum.Core.Tools;
using MySpectrum.Core.ViewModels;
using MySpectrum.Core.ViewModels.Base;

namespace MySpectrum.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly ICacheService _cacheService;
        private readonly IAlertService _alertService;

        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private IMvxAsyncCommand _signInCommand;
        public IMvxAsyncCommand SignInCommand => _signInCommand ?? (_signInCommand = new MvxAsyncCommand(HandleSignIn));

        private IMvxAsyncCommand _createAccountCommand;
        public IMvxAsyncCommand CreateAccountCommand => _createAccountCommand ?? (_createAccountCommand = new MvxAsyncCommand(HandleCreateAccount));

        public LoginViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService, ICacheService cacheService, IAlertService alertService) : base(logProvider, navigationService)
        {
            _cacheService = cacheService;
            _alertService = alertService;
        }

        private async Task HandleSignIn()
        {
            if (string.IsNullOrEmpty(Username?.Trim()))
            {
                _alertService.ShowMessage(Constants.Strings.EmailRequired);
                return;
            }

            if (string.IsNullOrEmpty(Password?.Trim()))
            {
                _alertService.ShowMessage(Constants.Strings.PasswordRequired);
                return;
            }

            var userResult = await _cacheService.GetUserAsync(Username);
            if (userResult == null)
            {
                _alertService.ShowMessage(Constants.Strings.InvalidCredentials);
                return;
            }

            if (Password != userResult.Password)
            {
                _alertService.ShowMessage(Constants.Strings.InvalidCredentials);
                return;
            }

            Username = string.Empty;
            Password = string.Empty;
            await NavigationService.Navigate<UserListViewModel>();
        }

        private Task HandleCreateAccount()
        {
            return NavigationService.Navigate<CreateAccountViewModel>();
        }
    }
}
