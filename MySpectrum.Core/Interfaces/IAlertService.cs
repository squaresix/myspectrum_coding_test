﻿using System;
namespace MySpectrum.Core.Interfaces
{
    public interface IAlertService
    {
        void ShowMessage(string message);
    }
}
