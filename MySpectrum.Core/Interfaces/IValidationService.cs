﻿using System;
using MySpectrum.Core.Models;

namespace MySpectrum.Core.Interfaces
{
    public interface IValidationService
    {
        bool IsValidEmail(string value);
        ValidationResultModel ValidatePassword(string value);
    }
}
