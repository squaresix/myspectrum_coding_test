﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MySpectrum.Core.Models;

namespace MySpectrum.Core.Interfaces
{
    public interface ICacheService
    {
        Task SetUserAsync(UserAccountModel userAccountModel);
        Task<UserAccountModel> GetUserAsync(string username);
        Task<List<UserAccountModel>> GetAllUsersAsync();
    }
}
