﻿using System;
using System.Threading.Tasks;
using MySpectrum.Core.Interfaces;
using MySpectrum.Core.Models;
using Akavache;
using System.Reactive.Linq;
using System.Collections.Generic;
using System.Linq;

namespace MySpectrum.Core.Services
{
    public class CacheService : ICacheService
    {
        public async Task SetUserAsync(UserAccountModel userAccountModel)
        {
            await BlobCache.Secure.InsertObject(userAccountModel.Username.ToLower(), userAccountModel);
        }

        public async Task<UserAccountModel> GetUserAsync(string username)
        {
            try
            {
                return await BlobCache.Secure.GetObject<UserAccountModel>(username.ToLower());
            }
            catch
            {
                return null;
            }
        }

        public async Task<List<UserAccountModel>> GetAllUsersAsync()
        {
            var results = await BlobCache.Secure.GetAllObjects<UserAccountModel>();
            return results.ToList();
        }
    }
}
