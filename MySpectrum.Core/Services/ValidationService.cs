﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using MySpectrum.Core.Interfaces;
using MySpectrum.Core.Models;

namespace MySpectrum.Core.Services
{
    public class ValidationService : IValidationService
    {
        private const string _emailRegex = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";
        private const string _letterAndDigitRegex = "[a-z]+\\d+|\\d+[a-z]+";
        private const string _sequenceRepetitionRegex = "(\\w{2,})\\1";

        private const string _lengthError = "Password must be between 5 and 12 characters long.";
        private const string _letterAndDigitError = "Password must contain both a letter and a digit.";
        private const string _repeatSequenceError = "Password must not contain any sequence of characters immediately followed by the same sequence.";

        public bool IsValidEmail(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            var regex = new Regex(_emailRegex);
            var regexMatch = regex.Match(value);
            return regexMatch.Success;
        }

        public ValidationResultModel ValidatePassword(string value)
        {
            var errors = new List<string>();

            if (value?.Length < 5 || value?.Length > 12)
            {
                errors.Add(_lengthError);
            }

            var letterAndDigitRegex = new Regex(_letterAndDigitRegex);
            var letterandDigitMatch = letterAndDigitRegex.Match(value);
            if (!letterandDigitMatch.Success)
            {
                errors.Add(_letterAndDigitError);
            }

            var repeatRegex = new Regex(_sequenceRepetitionRegex);
            var repeatMatch = repeatRegex.Match(value);
            if (repeatMatch.Success)
            {
                errors.Add(_repeatSequenceError);
            }

            return new ValidationResultModel
            {
                IsValid = !errors.Any(),
                Errors = errors
            };
        }
    }
}
