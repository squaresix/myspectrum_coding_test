﻿using System;

namespace MySpectrum.Core.Tools
{
    public static class Constants
    {
        public static class Strings
        {
            public const string SignUp = "Create Account";
            public const string CurrentUsers = "Current Users";
            public const string AccountAlreadyExists = "An account with this username already exists";
            public const string EmailRequired = "Email or ID is required";
            public const string PasswordRequired = "Password is required";
            public const string PasswordsDontMatch = "Passwords do not match";
            public const string EnterValidEmail = "Enter a valid Email ID";
            public const string InvalidCredentials = "Invalid credentials";
            public const string SignInHere = "Sign in here";
            public const string Cancel = "Cancel";
        }
    }
}
